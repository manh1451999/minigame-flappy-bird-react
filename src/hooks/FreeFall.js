import React, { useEffect, useRef, useState } from 'react'

export default function FreeFall({
    setValue, 
    a=9.8, 
    vOrigin= 0, 
    maxValue,
    minValue
}) {

    const timeLoop = useRef();
    const t= useRef(0);
    const [status, setStatus] = useState();


    const stop = ()=>{
        t.current= 0
        clearInterval(timeLoop.current);
        
    }

    useEffect(() => {
        if(status>0){
            timeLoop.current= setInterval(() => {
                t.current = t.current+0.1
                setValue(value=>{
                    console.log(`t`, t.current)
                    let newValue=  value+vOrigin*t.current+0.5*a*t.current*t.current // chuyển động nhanh dần đều y= y0+ vot + 1/2*at^2
                    let newValueCheck
                    if(minValue) newValueCheck = Math.max(newValue, minValue)
                    if(maxValue) newValueCheck = Math.min(newValue, maxValue)
                    if(newValue!==newValueCheck)  stop()
                    
                    return newValue
                })
            }, 50)
        }
       

    }, [status])


    const start = ()=>{
        setStatus(status=>status+1)
    }

   

    
    return {
        stop, start
    }
}
