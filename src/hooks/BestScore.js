import { useEffect, useState } from 'react'
import { GAME_OVER } from '../constans/game'
import { useGame } from '../contexts/game'

export default function BestScore() {
    const [bestScore, setBestScore] = useState( localStorage.getItem('bestScore') || 0)
    const {status, score} = useGame()

    useEffect(()=>{
        if(status=== GAME_OVER){
            let best = localStorage.getItem('bestScore') || 0;
            if(score> best){
                localStorage.setItem('bestScore', score);
                 setBestScore(score)
            }
        }
    }, [status])
    return {
        bestScore
    }
}
