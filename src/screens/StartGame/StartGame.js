import React, { memo } from 'react'
import startGameImg from '../../images/getReady.png'
import tutorialImg from '../../images/tutorial.png'
import './StartGame.css'
function StartGame() {
    return (
        <div className="start-game">
            <div className="getReady">
                <img alt="image" src={startGameImg} />
            </div>

            <div className="tutorial">
                <img alt="image" src={tutorialImg} />
            </div>

           
        </div>
    )
}

export default memo(StartGame)