import React, { useEffect, useState } from 'react'
import { NEW_GAME } from '../../constans/game'
import { useBird } from '../../contexts/bird'
import { useGame } from '../../contexts/game'
import { usePipe } from '../../contexts/pipe'
import BestScore from '../../hooks/BestScore'
import titleGameOverImg from '../../images/gameOver.png'
import playAgainImg from '../../images/playAgain.png'
import scoreBoardImg from '../../images/scoreBoard.png'
import Score from '../Score/Score'
import './GameOver.css'



export default function GameOver() {

    const { status, setStatus,  score, setScore } = useGame()
    const { initNewgame: initBird } = useBird()
    const { initNewgame: initPipes } = usePipe()
    const {bestScore} = BestScore()
    const [display, setDisplay] = useState(false)

    const playAgain = () => {
        setStatus(NEW_GAME)
        setScore(0)
        initPipes()
        initBird()
    }

    useEffect(() => {
        setTimeout(()=>{
            setDisplay(true)
        }, 500 )
    }, [])


    if(!display) return <div></div>
    return (
        <div className='game-over' >
            <div className="title-over">
                <img alt="image" src={titleGameOverImg} />
            </div>
            <div className="wrap-table">

                <div className='scoreBoard'>
                    <img alt="image" src={scoreBoardImg} />
                    <div className='score-end-game'>
                         <Score score={score}  />
                    </div>

                    <div className='best-score'>
                         <Score score={bestScore}  />
                    </div>
                </div>

                <div className="playAgain" onClick={playAgain}>
                    <img alt="image" src={playAgainImg} />
                </div>

            </div>

        </div>
    )
}
