import React, { memo, useEffect, useMemo } from 'react'
import { LEFT_BIRD } from '../../constans/game'
import { useBird } from '../../contexts/bird'
import BirdImage from '../../images/bird.png'
import './Bird.css'

function Bird() {
    const { y } = useBird()
    const { r } = useBird()

    useEffect(() => {
        console.log(`render bird`)
    }, [])
    return (
        <div className='bird'
            style={{
                backgroundImage: `url(${BirdImage})`,
                backgroundSize: "cover",
                top: y,
                left: LEFT_BIRD,
                transform: `rotate(${r}deg)`,
                transition: `transform 200ms, top 200ms`
            }}>

        </div>
    )
}

export default memo(Bird)