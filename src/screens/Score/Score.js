import React, { memo } from 'react';
import { useGame } from '../../contexts/game';
import './Score.css';

function Score({score}) {
    const { score:scoreConText, setScore} = useGame();
    const scoreString = score?score.toString():scoreConText.toString();
    return (
        <div className='score'>
            {scoreString.split('').map((e, i)=>{
            return <img alt="image" key={i} src={`/images/${e}_big.png`} />
            })}
        </div>
    )
}

export default memo(Score)