import React, { memo } from 'react'
import { HEIGHT_PIPE, SPACE_PIPE_HORIZONTAL, SPACE_PIPE_VERTICAL, WIDTH_PIPE } from '../../constans/game'
import { usePipe } from '../../contexts/pipe'
import PipeBotImage from '../../images/pipe-bottom.png'
import PipeTopImage from '../../images/pipe-top.png'
import './Pipe.css'
 function Pipe() {

    const { x,  pipesView } = usePipe()

    return (
        <div className='pipe' >
            {pipesView.map(({ top, index }) => (
                <div
                    key={`pipe-${index}`}
                    style={{
                        position: 'relative'
                    }}>
                    <div className='top-pipe'
                        key={`pipe-top-${index}`}
                        style={{
                            width: WIDTH_PIPE,
                            height: top,
                            backgroundImage: `url(${PipeTopImage})`,
                            backgroundPosition: 'bottom',
                            top: 0,
                            left: x + index * SPACE_PIPE_HORIZONTAL,
                            transition: `left 150ms `
                        }}></div>
                    <div className='bot-pipe'
                    key={`pipe-down-${index}`}
                        style={{
                            width: WIDTH_PIPE,
                            height: HEIGHT_PIPE,
                            backgroundImage: `url(${PipeBotImage})`,
                            backgroundSize: "cover",
                            top: top + SPACE_PIPE_VERTICAL,
                            left: x + index * SPACE_PIPE_HORIZONTAL,
                            transition: `left 150ms `
                            
                        }}></div>
                </div>))
            }

        </div >
    )
}

export default memo(Pipe)

