import React, { memo, useCallback, useEffect, useRef } from 'react'
import { DECREASE_Y, GAME_OVER, HEIGHT_BIRD, HEIGHT_FOREGROUND, HEIGHT_VIEW, LEFT_BIRD, NEW_GAME, PLAYING, SPACE_PIPE_VERTICAL, WIDTH_BIRD, WIDTH_PIPE } from '../constans/game'
import BirdProvider, { useBird } from '../contexts/bird'
import GameProvider, { useGame } from '../contexts/game.js'
import PipeProvider, { usePipe } from '../contexts/pipe'
import FreeFall from '../hooks/FreeFall'
import BgImage from '../images/bg.png'
import Bird from './Bird/Bird'
import Foreground from './Foreground/Foreground'
import './Game.css'
import GameOver from './GameOver/GameOver'
import Pipe from './Pipe/Pipe'
import Score from './Score/Score'
import StartGame from './StartGame/StartGame'

function GameImpl() {

    const fallLoop = useRef()
    const pipeLoop = useRef()
    const upWaitGame = useRef()
    const freeFallLoop = useRef()
    const { status, setStatus, score, setScore, ratio} = useGame()
    const { y, setY, r, setR, inPipe, setInPipe } = useBird()
    const {x, running, pipesView } = usePipe()
    const {start: startFreeFall, stop: stopFreeFall} = FreeFall({
        maxValue: HEIGHT_VIEW-HEIGHT_FOREGROUND-HEIGHT_BIRD,
        setValue: setY,
    })


    useEffect(() => {
        if(status!==GAME_OVER){
            document.addEventListener('keydown', handleKeyPress)
            document.addEventListener('touchstart', handleKeyPress)
            // document.addEventListener('mousedown', handleKeyPress)
        }

        return () => {
            document.removeEventListener('keydown', handleKeyPress)
            document.removeEventListener('touchstart', handleKeyPress)
            // document.removeEventListener('mousedown', handleKeyPress)
        }
    }, [status])

    useEffect(() => {
        checkGameOver()
    }, [x, pipesView])

    useEffect(() => {
        if(status === NEW_GAME){
            upWaitGame.current = setInterval(() => {
                setY(y => (y + 10))
                setTimeout(() =>{
                    setY(y => (y - 10))
                }, 225)
            }, 450)
        }
        else {
            clearInterval(upWaitGame.current)
        }

        return ()=> clearInterval(upWaitGame.current)
    }, [status])


    const handleKeyPress =  useCallback(
        (e) => {
            start()
            if (e.keyCode == 32 || e.keyCode == 13) {
                fly();
            }
    
            if (e.keyCode == null) {
                fly();
            }
        },
        [status]
    )


    const start = useCallback(() => {
        if (status === PLAYING) return 
        if (status === NEW_GAME) {
            setStatus(PLAYING)
            fallLoop.current= setInterval(() => {
                fall(14)
            }, 100)
            pipeLoop.current= setInterval(() => {
                running(5)
            }, 100)
        }
    }, [status]
    )

    const fall = useCallback((i) => {
        setY(y => (y + i))
        setR(r=>Math.min(r+8, 20))

    }, []
    )

    
    const fly = useCallback(() => {
        setY(y => (y - DECREASE_Y))
        setR(-25)
    }, []
    )


    const checkGameOver= useCallback(()=>{
        // const pipeView= pipes.filter(({index})=>(x + index * 200)>-50 && (x + index * 200)<300)
        let inPipeNow= false;

        if( y+HEIGHT_BIRD > HEIGHT_VIEW-HEIGHT_FOREGROUND ) gameOver()

        pipesView.map(({top : topPipe,index})=>{
            let xPipe = x + index * 200
            if( (LEFT_BIRD+WIDTH_BIRD) > xPipe && LEFT_BIRD < (xPipe+WIDTH_PIPE) ){
                 inPipeNow=true;
                if ( y< topPipe || (y + HEIGHT_BIRD) >  (topPipe + SPACE_PIPE_VERTICAL)){
                    if ( y< topPipe) setY(topPipe)
                    gameOver()

                }
            }

        })

        if(inPipe != inPipeNow){
            if(inPipe==true && inPipeNow==false) {
                setScore(score=>score+1)
            }
            setInPipe(inPipeNow)
        }
    }, [x,pipesView]
    )



    const gameOver = useCallback(()=>{
        console.log("game over")
        setStatus(GAME_OVER)
        // startFreeFall()
        clearInterval(fallLoop.current)
        clearInterval(pipeLoop.current)
        setTimeout(()=>{
            setY(HEIGHT_VIEW-HEIGHT_FOREGROUND-HEIGHT_BIRD);
            setR(80)
        }, 150)
        document.removeEventListener('keydown', handleKeyPress)
        document.removeEventListener('touchstart', handleKeyPress)
        // document.removeEventListener('mousedown', handleKeyPress)
    },[status]
    )

    return (
        <div className="app" >
            <div className="game"
                style={{
                    backgroundImage: `url(${BgImage})`,
                    backgroundSize: "cover",
                    transform: `scaleY(${ratio}) scaleX(${ratio})`
                }}>
                {status == NEW_GAME && <StartGame />}
                {status == GAME_OVER && <GameOver />}
                {status !== GAME_OVER && <Score />}
               
                <Bird />
                <Pipe />
                <Foreground />
            </div >
        </div >


    )
}

function Game() {
    return (
        <GameProvider>
            <BirdProvider>
                <PipeProvider>
                    <GameImpl />
                </PipeProvider>
            </BirdProvider>
        </GameProvider >)
}

export default memo(Game)