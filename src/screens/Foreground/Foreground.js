import React, { memo, useEffect, useRef, useState } from 'react'
import { GAME_OVER } from '../../constans/game'
import { useGame } from '../../contexts/game'
import FgImage from '../../images/fg.png'
import './Foreground.css'
function Foreground() {
    const { status } = useGame()
    const [positionBG, setPositionBG] = useState(0)
    const gbInterval = useRef()
    useEffect(()=>{
        if(status === GAME_OVER) clearInterval(gbInterval.current)
        else gbInterval.current= setInterval(()=>{
            setPositionBG(x=>(x+5)%100000) 
        }, 100)


        return ()=>{
            clearInterval( gbInterval.current)
        }
    }, [status])
    return (
        <div className='fore-ground'
            style={{
                backgroundImage: `url(${FgImage})`,
                backgroundSize: "cover",
                backgroundPosition: `${positionBG}px 0px`,
                transition: `all 200ms linear`
            }}>

        </div>
    )
}

export default memo(Foreground)