import React, { useCallback, useContext, useEffect, useMemo, useState } from "react";
import { HEIGHT_PIPE, SPACE_PIPE_HORIZONTAL, START_LEFT_PIPE, WIDTH_PIPE, WIDTH_VIEW } from "../constans/game";
export const pipeContext = React.createContext();
export const usePipe = () => useContext(pipeContext)

export default function PipeProvider({ children }) {

    const initPipes =()=>{
        return [
            {
                top: 150 + Math.floor(50 * Math.random()),
                left: 300,
                index:0
            },
            {
                top: 150 + Math.floor(50 * Math.random()),
                left: 300,
                index:1
            },
            {
                top: 150 + Math.floor(50 * Math.random()),
                left: 300,
                index:2
            },
            {
                top: 150 + Math.floor(50 * Math.random()),
                left: 300,
                index:3
            }
        ]
    }
    const [x, setX] = useState(START_LEFT_PIPE);
    const [pipes, setPipes] = useState(initPipes());
    const [pipesView, setPipesView] = useState([])

    const initNewgame= useCallback(() => {
        setX(START_LEFT_PIPE)
        setPipes(initPipes())
    }, []
    )

    useEffect(()=>{
        let pipesView= pipes.filter(({index})=>(x + index * SPACE_PIPE_HORIZONTAL)>-WIDTH_PIPE && (x + index * SPACE_PIPE_HORIZONTAL)<WIDTH_VIEW)
        setPipesView(pipesView)
    },[pipes,x])

    const running = useCallback((i) =>{
        setX(x => x - i)
    }, []
    )

    const  generate = useCallback(() =>{

        if (x + (pipes[pipes.length-1].index-2) * SPACE_PIPE_HORIZONTAL < 0) {
            let newPipes = [...pipes]
            newPipes = newPipes.slice(1)
            newPipes.push({
                top: (HEIGHT_PIPE-50) + Math.floor(50 * Math.random()),
                 left: START_LEFT_PIPE,
                index:pipes[pipes.length-1].index+1
            })
            setPipes(pipes=>newPipes)
        }
    },[x, pipes]
    )




    useEffect(() => {
        generate()
    }, [x])

    const value = useMemo(()=>({
        x,
        setX,
        running,
        pipes,
        setPipes,
        generate,
        pipesView,
        initNewgame
    }), [x, pipes, pipesView])

    return (
        <pipeContext.Provider
            value={value}
        >
            {children}
        </pipeContext.Provider>
    )
}



