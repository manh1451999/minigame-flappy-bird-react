import React, { useContext, useLayoutEffect, useState } from "react";
import { HEIGHT_VIEW, NEW_GAME } from "../constans/game";

export const gameContext = React.createContext();
export const useGame = () => useContext(gameContext)

export default function GameProvider({ children }) {

    const [status, setStatus] = useState(NEW_GAME);
    const [score, setScore] = useState(0);
    const [ratio, setRatio ] = useState(1)

    useLayoutEffect(() => {
        function updateSize() {
            setRatio(0.95*document.documentElement.clientHeight/HEIGHT_VIEW || 1) 
        }
        window.addEventListener('resize', updateSize);
        updateSize();
        return () => window.removeEventListener('resize', updateSize);
    }, []);

    
    return (
        <gameContext.Provider
            value={{
                status,
                setStatus,
                score,
                setScore,
                ratio
            }}
        >
            {children}
        </gameContext.Provider>
    )
}

