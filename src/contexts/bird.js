import React, { useContext, useMemo, useState } from "react";
export const birdContext = React.createContext();
export const useBird = () => useContext(birdContext)

export default function BirdProvider({ children }) {

    const [r, setR] = useState(0);
    const [y, setY] = useState(200);
    const [inPipe, setInPipe] = useState(false);

    // const fly = () => {
    //     console.log('fly')
    //     setY(y => y - 25)
    // }

    const initNewgame = () => {
        setR(0)
        setY(200)
        setInPipe(false)
    }

    const value = useMemo(()=>({
        r,
        setR,
        y,
        setY,
        initNewgame,
        inPipe, 
        setInPipe
    }), [r, y, inPipe])

    return (
        <birdContext.Provider
            value={value}
        >
            {children}
        </birdContext.Provider>
    )
}



