export const PLAYING = "PLAYING"
export const NEW_GAME = "NEW_GAME"
export const GAME_OVER = "GAME_OVER"

export const WIDTH_BIRD= 38;
export const HEIGHT_BIRD= 26;
export const LEFT_BIRD= 150-WIDTH_BIRD/2;
export const TOP_BIRD= 200;

export const WIDTH_PIPE= 50;
export const HEIGHT_PIPE= 200;
export const START_LEFT_PIPE= 300;


export const WIDTH_VIEW= 300;
export const HEIGHT_VIEW= 500;


export const HEIGHT_FOREGROUND= 100;
export const SPACE_PIPE_HORIZONTAL= 200;
export const SPACE_PIPE_VERTICAL= 100

export const DECREASE_Y = 60
export const TRANSITION_PIPE = 150  // chưa dùng
export const TRANSITION_BIRD = 200  // chưa dùng



